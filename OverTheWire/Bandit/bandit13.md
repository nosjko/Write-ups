# Bandit 13

Link: [https://overthewire.org/wargames/bandit/bandit13.html](https://overthewire.org/wargames/bandit/bandit13.html)

## Challenge Details

The password for the next level is stored in the file `data.txt`, which is a hexdump of a file that has been `repeatedly compressed`. For this level it may be useful to create a directory under /tmp in which you can work using mkdir. For example: mkdir /tmp/myname123. Then copy the datafile using cp, and rename it using mv (read the manpages!).

## Solution

For this challenge, we'll be using quite a variety of tools to figure out format of files and to inflate/decompress the files.

First off, we need to find out what kind of file we're dealing with, using the `file` command as well as the `cat` command will be very instrumental in completing this challenge.

First off, let's make a new directory in `/tmp` to work with our files in.

We can `cd` to `/tmp` and make a new directory with `mkdir`.

Now we need to copy the `data.txt` file to out directory, we can `cd` into our new directory and run `cp ~/data.txt .` to copy the `data.txt` to our current working directory.

Now we can run the `file` command on the `data.txt` tells us that it's an `ASCII text`, this doesn't tell us much so we can run the `cat` command on the `data.txt` file to see that it's a hex dump.

Now that we know it's a hex dump, we can use the `xxd` tool to revert the file from a hex dump to a binary file using the `-r` option.

The command we want to run is `xxd -r data.txt > test.txt`.

This will revert the `data.txt` file from hex to binary and store the result of that command in a new file called `test.txt`.

Now we can run the `file` command on `test.txt` and we can see from the output, `test.txt: gzip compressed data, was "data2.bin"` that test.txt was originally called data2.bin and is a `gzip compressed file`.

First, we have to rename the `test.txt` file to `test.gz` by running `mv test.txt test.gz` so that we can use the `gunzip` command to inflate `gzip archive`.

The command we want to run is `gunzip test.gz` and once we run it, we inflate the archive and get a file called `test`.

Running the `file` command on `test` gives us the output `test: bzip2 compressed data` and tells us that the file is a `bzip2 compressed file`.

Now we need to rename the `test` file to `test.bz2` by running command `mv test test.bz2` so that we can use `bunzip2` command with the `-d` option to decompress the `bzip2 archive`.

Now we have a file called `test` again, and running `file` on `test` tells us that it is again a `gzip compressed file`. Repeating the steps above, we rename and decompress the file to get a `tar archive` as per the output of the file command on the new `test` file, `test: POSIX tar archive (GNU)`.

Now we know it's a `tar archive` we rename the `test` file using the command `mv test test.tar` and now we can use the `tar` command with the `-xf` options, `x` to extract files and `f` to indicate that it is an archive.

Running the command `tar -xf test.tar` we extract a file called `data5.bin` and running the `file` command on this file tells us that it's another `tar archive`.

Repeating the steps above to unarchive the `tar archive` we get another `bzip2 compressed file` called `data6.bin`.

Once we decompress `data6.bin`, we get another file called `data6` and running the `file` command on this tells us that its another `tar archive`.

Repeating the above steps to extract the files in the `tar archive` we get a new file called `data8.bin` and running `file` on this tells us that it is a `gzip archive`.

Repeating the steps above (again), we get a new file called `data8`. Running `file` on `data8` tells us that it is an `ASCII text` file and running `cat` on `data8` finally gives us the password for the `bandit13` user!

About time.

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit13@bandit.labs.overthewire.org -p 2220
Password: 8ZjyCRiBWFYkneahHwxCv3wb2a1ORpYL
        </pre>
</details>
