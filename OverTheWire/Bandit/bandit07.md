# Bandit 7

Link: [https://overthewire.org/wargames/bandit/bandit7.html](https://overthewire.org/wargames/bandit/bandit7.html)

## Challenge Details

The password for the next level is stored somewhere on the server and has all of the following properties:

* owned by user bandit7
* owned by group bandit6
* 33 bytes in size

## Solution

This challenge is similar to the challenge in `bandit6`, except this time we are specifying other criteria to filter our `find` command by and from a different directory to start the command. Namely we are filtering by user, group and size of the file.

We can again consult the man page for the `find` command, these options in particular are the ones that we will use:

* `-user bandit7`
  * This option will let us find all files owned by the user `bandit7`
* `-group bandit6`
  * This option will let us find all files owned by the group `bandit6`
* `-size 33c`
  * This options will let us find all files that are size 33 bytes

The command that we run is `find / -user bandit7 -group bandit6 -size 33c  2>/dev/null`.

Now the reason that we append `2>/dev/null` to the end of the command is to basically redirect all the errors that we'd get into `/dev/null` which is essentially the linux black hole where we put all the stuff we dont want. If we were to run the command without `2>/dev/null` we would also get a list of files that are owned by `bandit7`, in a group with `bandit6` and `33 byes` however we could not access them. Running the command with `2>/dev/null` would sent all `Permission denied` errors to the bin.

When we run the command, the result that we get back is that the file with the password is `/var/lib/dpkg/info/bandit7.password`.

Running `cat` on this file will give us the password to the `bandit7` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit7@bandit.labs.overthewire.org -p 2220
Password: HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs
        </pre>
</details>
