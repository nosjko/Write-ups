# Bandit 11

Link: [https://overthewire.org/wargames/bandit/bandit11.html](https://overthewire.org/wargames/bandit/bandit11.html)

## Challenge Details

The password for the next level is stored in the file `data.txt`, which contains `base64` encoded data

## Solution

For this challenge, we'll need to make use of the `base64` command line utility which we can use to both encode plain text to the base64 format or decode base64 encoded strings to plain text.

Specifically, we want to use the `-d` options to decode the base64 encoded string to plaintext.

We run the command `base64 -d data.txt` which gives us the password to the `bandit11` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit11@bandit.labs.overthewire.org -p 2220
Password: IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR
        </pre>
</details>
